// function to find max number in a array
function maxelement(arr){
    let max = arr[0];
    arr.forEach((el)=>{
        //if  (el > max) 
        //max = el
        max = Math.max(max,el)
    })
    return max
}



const a = [3,5,5,7,9,5]

console.log(maxelement(a))




// reverse a number
function reverse(num){
    let rev = 0
    while (num){
        rev = rev*10 + num%10
        num = Math.floor(num/10)
    }
    return rev

}

console.log(reverse(3456))

// while loop
let num =0;
while(num <5){

    console.log("hello"+num)
    num=num+1; 
}

// find the count of digits in a num
function count(num){
    let count = 0
    while (num){
        count+=1
        num = Math.floor(num/10)
    }
    return count

}

console.log(count(34568))

// sum of the digits of a number 
function sumofdigits(num){
    let sum=0
    while(num){
        sum+=num%10
        num = Math.floor(num/10)
    }
    return sum
}

console.log(sumofdigits(345))

//product of the digits of a number
function productofdigits(num){
    let pro=1
    while(num){
        pro*=num%10
        num = Math.floor(num/10)
    }
    return pro
}

console.log(productofdigits(345))


// write a program  check if number is palandrome
function palandrome(num){
    if (reverse(num) == num)
        return "Palandrome"
    else 
        return "Not a Palandrome"

}

console.log(palandrome(134))

// write a program to reverse a function

function reversestring(str){
    let rev = ""
    for(let i = str.length-1;i >=0;i--){
        rev += str[i]
    }
    return rev
}
console.log(reversestring("Hello World"))

// write a program to check a string is palandrome
function palandromestring(str){
    return  reversestring(str) === str
        }
        
    


console.log(palandromestring("mood"))

console.log("moon".split("").reverse().join(""))


function prime(num){

    if (num == 1)
        return "Neither prime or not composite"
    for(let i = 2 ; i <= Math.floor(Math.sqrt(num));i++){
        if  (num%i == 0 )
            return false
    }
    return true
}

console.log(prime(139))

/*
print the factors of the number
sum of factors of the number
find the perfect number
Perfect Number == Sum of factors of a number is equal to given number
print perfect numbers between 100 to 999
*/

// print the factors of a number
function factorsofnumber(num){
    let factors = ""
    for(let i = 1;i <= Math.floor(num/2);i++){
        if (num%i==0){
            factors+= i + " "
        }
    }
    return factors
}

console.log(factorsofnumber(10))

// sum of factors of a number 
function sumoffactors(num){
    let sumoffactors = 0
    for (let i = 1;i <= Math.floor(num/2);i++){
        if (num%i==0)
            sumoffactors+=i
    }
    return sumoffactors
}

console.log(sumoffactors(12))

// perfect number or not
function perfectnumber(num){
    
    return sumoffactors(num) == num
}

console.log(perfectnumber(28))

// print perfect numbers between 100 and 999
let text = ''
for (let i= 100 ; i <= 999 ; i++){
    if (perfectnumber(i))
        text+= i + " "
}
console.log(text)