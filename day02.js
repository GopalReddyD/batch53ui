//for loop 

for(let i = 0 ; i < 5 ;i++){
    console.log(i)
}

const phones = ['apple','samsung','oppo','vivo']

// forEach
phones.forEach((ele)=>{
    console.log(ele)
})


// for in : to acess keys for objects and indexes for arrays
 for(el in phones){
    console.log(phones[el])
 }

 // for of : to acess values 
 for(el of phones){
    console.log(el)
 }

// function should always return and should not print
/* syntax for function : function funcname(parameter1,paramters2,...){
        statements
}
*/
//function for squrare and cube

function square(num){

    return num**2
}

function cube(num){

    return num**3
}

let squ = square(5)
let cub = cube(5)

console.log(squ,cub)

// function to find max number in a array
function maxelement(arr){
    let max = arr[0];
    arr.forEach((el)=>{
        //if  (el > max) 
        //max = el
        max = Math.max(max,el)
    })
    return max
}



const a = [3,5,5,7,9,5]

console.log(maxelement(a))

//math object 

console.log(Math.max(6,23))
console.log(Math.min(6,23))
console.log(Math.floor(9.89))
console.log(Math.ceil(45.23))
console.log(Math.pow(7,3))
console.log(Math.round(7.5456))
console.log(Math.ceil(Math.random()*100))
console.log(Math.max(4,5,64,34,23))

// function to find even or odd

function evenodd(num){
    if (num%2 === 0){
        return "even"
    }
    else 
        return "odd"
}

console.log(evenodd(4))
console.log(evenodd(5))


console.log(Math.max(...a))  // ... used to open the array  


//sum of an array
function sum(arr){
    let sum = 0
    arr.forEach(element => {
        sum+=element
    });
    return sum
}

console.log(sum(a))


class Mobile{

    constructor(name,ram,storage){
        this.name = name;
        this.ram = ram;
        this.storage = storage;
    
    }


}

m1 = new Mobile("Vivo",4,128)

console.log(m1.name)

console.log(`The name of the phone is ${m1.name} and the ram is ${m1.ram}GB as for storage it has ${m1.storage}GB`)

// write a function to reversa a number 





